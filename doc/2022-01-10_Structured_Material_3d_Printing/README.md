## README

### Credits:

- This talk was designed using the [RISE](https://rise.readthedocs.io/en/stable/index.html) (Reveal.js - Jupyter/IPython Slideshow Extension) notebook presentation mode.

- Templates and tips borrowed from Firas Moosvi's ["Customing RISE Slides" video](https://www.youtube.com/watch?v=dAxWpE7_v1A) and [repo](https://github.com/firasm/altair_talk).

### Requirements:

- Install jupyter_contrib_nbextensions:
    - `pip install jupyter_contrib_nbextensions` OR 
    - `conda install -c conda-forge jupyter_contrib_nbextensions`

- Install Javascript and CSS files
    - `jupyter contrib nbextension install --user`

- Install jupyter_nbextensions_configurator
    - `pip install jupyter_nbextensions_configurator` and `jupyter nbextensions_configurator enable --user`
    - `conda install -c conda-forge jupyter_nbextensions_configurator`

- Enable the "Split Cells Notebok", see image below

![img.png]

The instructions above are outlined in detailed [here](https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/install.html).

- Install PyVista and  'ipyvtklink'
  - `conda install -c conda-forge pyvista ipyvtklink`
